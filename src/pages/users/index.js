import React, { useState, useEffect } from 'react';
import { FlatList, View, Text, StyleSheet } from 'react-native';
import axios from 'axios';

const api = axios.create({
    baseURL: "http://10.89.234.155:5000/api/",
    header: {
        'accept': 'application/json',
    }
});

const Users = () => {
    const [users, setUsers] = useState([]);

    useEffect(() => {
    const fetchUsers = async () => {
        try {
            const response = await api.get("/external");
            const users = response.data.users; 
            setUsers(users);
        } catch (error) {
            console.error("Error fetching users:", error);
        }
    };

    fetchUsers();
    }, []);

return (
    <View style={styles.container}>  
        <Text style={styles.textUser}>Users</Text>
        <FlatList
            data={users}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }) => (
                <View style={styles.list}> 
                    <Text style={styles.textContainer}>{item.name}: {item.email}</Text>
                </View>
            )}
        />
    </View> 
);
};

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        marginTop: 35,
    },
    textUser:{
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 5,
    },
    textContainer:{
        backgroundColor: '#894de9',
        margin: 5,
        borderRadius: 8,
        fontSize: 16,
        fontWeight: '800',
        padding: 5
    }

})

export default Users;
