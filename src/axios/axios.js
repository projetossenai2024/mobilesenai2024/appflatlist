import axios from "axios";

const api = axios.create({
    baseURL: "http://localhost:5000/api/",
    header: {
        'accept': 'application/json',
    }
});

const sheets = {
    //User list
    getUsers: async () => {
        try {
            const response = await api.get("/external");
            return response.data.users; // Acessando o array 'users'
        } catch (error) {
            // Tratar erro apropriadamente
            console.error("Error fetching users:", error);
            throw error;
        }
    }
}


export default sheets;
